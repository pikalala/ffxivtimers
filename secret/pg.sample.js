
if (process.env.NODE_ENV === 'production') {
    module.exports = {
        "host": "localhost",
        "user": "postgres",
        "password": "password",
        "database": "database",
        "port": 5432
    };
} else {
    module.exports = {
        "host": "localhost",
        "user": "postgres",
        "password": "password",
        "database": "database",
        "port": 5432
    };
}

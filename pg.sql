
CREATE SEQUENCE public.ffxivtimers_id_seq
    INCREMENT 1
    START 1
    MINVALUE 1
    MAXVALUE 2147483647
    CACHE 1;

CREATE TABLE public.ffxivtimers
(
    id integer NOT NULL DEFAULT nextval('ffxivtimers_id_seq'::regclass),
    "webhookId" bigint NOT NULL,
    "guildId" bigint NOT NULL,
    "channelId" bigint NOT NULL,
    token text COLLATE pg_catalog."default" NOT NULL,
    url text COLLATE pg_catalog."default" NOT NULL,
    lang text COLLATE pg_catalog."default" NOT NULL,
    jumbona boolean NOT NULL,
    jumboeu boolean NOT NULL,
    jumbojp boolean NOT NULL,
    weekly boolean NOT NULL,
    daily boolean NOT NULL,
    gc boolean NOT NULL,
    ocean boolean NOT NULL,
    lala boolean NOT NULL,
    tt boolean NOT NULL DEFAULT false,
    test boolean NOT NULL DEFAULT false,
    CONSTRAINT ffxivtimers_pkey PRIMARY KEY (id)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

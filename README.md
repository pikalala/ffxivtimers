FFXIV Timers
============

This is a discord webhook bot for sending various notifications of ffxiv events, plus a website to display those timers.

You will need Node.js and PostgreSQL, along with a Discord application which you can make at https://discord.com/developers/applications/. Do the standard `yarn install` and set up your secrets in the `secret` folder. Create a database table using the `pg.sql` script.

There is no build pipeline because it is unnecessary for this project at this time. Kiss and stuff. Just `yarn run start`.

I will probably not accept most pull requests unless it is something I already planned on doing.

License
-------

MIT

> Copyright 2020 Lalachievements
> 
> Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
> 
> The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
> 
> THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

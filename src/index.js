const path = require('path');

const express = require('express');
const Squirrel = require('squirrelly');
const { Client } = require('pg');
const Cron = require('cron').CronJob;
const formurlencode = require('form-urlencoded').default;
const needle = require('needle');
const { Webhook, MessageBuilder} = require('discord-webhook-node');

const app = express();
const port = 3003;

app.use(express.static(path.join(__dirname, 'public')));
app.use(express.static(path.join(__dirname, 'build')));
app.set('views', path.join(__dirname, 'views'));
app.engine('html', Squirrel.renderFile);

const client = new Client(require('../secret/pg.js'));
client.connect();

let discordSecret = require('../secret/discord.js');

app.get('/', function(req, res) {
    let nextTriads = crons.tt.nextDates(3);
    let nextTriadEnds = crons.ttEndWarning.nextDates(3);
    let nextVerminions = crons.verminion.nextDates(12);
    let nextVerminionEnds = crons.verminionEndWarning.nextDates(12);
    let nextTriad = nextTriads.find(m => isTriadNow(new Date(m.format('x')*1))).format('x');
    let nextTriadEnd = nextTriadEnds.find(m => isTriadNow(new Date(m.format('x')*1))).format('x')*1+3600000;
    let nextVerminion = nextVerminions.find(m => isVerminionNow(new Date(m.format('x')*1))).format('x');
    let nextVerminionEnd = nextVerminionEnds.find(m => isVerminionEndNow(new Date(m.format('x')*1))).format('x')*1+3600000;
    let triadCurrent = nextTriad > nextTriadEnd;
    let verminionCurrent = nextVerminion > nextVerminionEnd;

    res.render('index.html', {
        page: '',
        jumbona: crons.jumbona.nextDates().format('x'),
        jumboeu: crons.jumboeu.nextDates().format('x'),
        jumbojp: crons.jumbojp.nextDates().format('x'),
        weekly: crons.weekly.nextDates().format('x'),
        daily: crons.daily.nextDates().format('x'),
        gc: crons.gc.nextDates().format('x'),
        ocean: crons.ocean.nextDates().format('x'),
        lalachievements: crons.lalachievements.nextDates().format('x'),
        triadCurrent: triadCurrent,
        verminionCurrent: verminionCurrent,
        tt: triadCurrent ? nextTriadEnd : nextTriad,
        verminion: verminionCurrent ? nextVerminionEnd : nextVerminion
    });
});

// triple triad: every other week from reset-reset
// verminion: 8am friday - 1am? tuesday (weekly or every other week?)

const iconUrls = {
    'jumbo': 'https://cdn.discordapp.com/attachments/546520452202364949/719599638944153760/cactaur1.png',
    'weekly': 'https://cdn.discordapp.com/attachments/546520452202364949/719604869371133962/nutkin.png',
    'daily': 'https://cdn.discordapp.com/attachments/546520452202364949/719621127777026088/screelunking1.png',
    'gc': 'https://cdn.discordapp.com/attachments/546520452202364949/719607460221943858/paissa2.png',
    'ocean': 'https://cdn.discordapp.com/attachments/546520452202364949/719602949931794482/shark.png',
    'lala': 'https://cdn.discordapp.com/attachments/546520452202364949/719611542655598602/tataru.png',
};

// todo load data from ikd tables instead of hard coded in case they expand ocean fishing
// IKDRouteTable
let oceanPattern = [
    1, 4, 2, 5, 3, 6,
    1, 4, 2, 5, 3, 6,
    4, 1, 5, 2, 6, 3,
    4, 1, 5, 2, 6, 3,
    2, 5, 3, 6, 1, 4,
    2, 5, 3, 6, 1, 4,
    5, 2, 6, 3, 4, 1,
    5, 2, 6, 3, 4, 1,
    3, 6, 1, 4, 2, 5,
    3, 6, 1, 4, 2, 5,
    6, 3, 4, 1, 5, 2,
    6, 3, 4, 1, 5, 2
];

// IKDRoute
let oceanRoutes = [
    '',
    'Southern Night > Galadion Day > Northern Sunset',
    'Southern Day > Galadion Sunset > Northern Night',
    'Southern Sunset > Galadion Night > Northern Day',
    'Galadion Night > Southern Day > Rhotano Sunset',
    'Galadion Day > Southern Sunset > Rhotano Night',
    'Galadion Sunset > Southern Night > Rhotano Day'
];

app.get('/ocean', function(req, res) {
    let next = new Date(crons.ocean.nextDates().format('x')*1).getTime();
    let times = [];
    for (let i = -1; i < 25; i++) {
        times.push({
            time: next + 7200000*i
        });
    }
    times = times.map(function(time) {
        let routeIndex = (Math.floor(time.time / 7200000) + 16) % 72;
        return {
            time: time.time,
            route: oceanRoutes[oceanPattern[routeIndex]]
        };
    });
    res.render('ocean.html', {
        page:'ocean',
        times: times
    });
});
app.get('/webhook', function(req, res) {
    res.render('webhook.html', {page:'webhook'});
});
app.get('/authorize', async function(req, res) {
    if (!req.query.code || !req.query.code.length || !req.query.state || !req.query.state.length) {
        res.status(400).render('webhook.html', {page:'webhook'});
    }
    let code = req.query.code;
    let state = req.query.state.split(',');
    if (state.length < 3) {
        res.status(400).render('webhook.html', {page:'webhook'});
    }
    needle.post('https://discord.com/api/oauth2/token', formurlencode(Object.assign({
        grant_type: 'authorization_code',
        code: code,
        redirect_uri: 'https://ffxivtimers.com/authorize'
    }, discordSecret)), function(err, resp, body) {
        if (err) {
            console.error(err);
            return res.render('webhook.html');
        }
        if (resp.statusCode !== 200) {
            console.error(body);
            return res.render('webhook.html');
        }
        client.query(`insert into ffxivtimers (
        "webhookId", "guildId", "channelId", "token", "url", "lang",
        "jumbona", "jumboeu", "jumbojp", "tt",
        "weekly", "daily", "gc", "ocean", "lala"
        ) values (
        $1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12, $13, $14, $15
        )`, [
            body.webhook.id, body.webhook.guild_id, body.webhook.channel_id, body.webhook.token, body.webhook.url, state[0],
            state.indexOf('jumbona') > -1, state.indexOf('jumboeu') > -1, state.indexOf('jumbojp') > -1, state.indexOf('tt') > -1,
            state.indexOf('weekly') > -1, state.indexOf('daily') > -1, state.indexOf('gc') > -1, state.indexOf('ocean') > -1, state.indexOf('lala') > -1,
        ], (err) => {
            if (err) {
                console.log(err);
                res.render('webhook.html');
            } else {
                const hook = new Webhook(body.webhook.url);
                hook.setUsername('FFXIV Timers');
                //hook.setAvatar('https://cdn.discordapp.com/embed/avatars/0.png');
                let embed = new MessageBuilder()
                    .setDescription('This channel is now subscribed to FFXIV Timers.')
                    .setColor(4906554)
                    .setURL('https://ffxivtimers.com/ocean');
                hook.send(embed).catch(err => console.error(err));
                res.render('index.html');
            }
        });
    });
});

function sendWebhooks(field, embed) {
    client.query(`SELECT * FROM ffxivtimers WHERE ${field} = true`, (err, result) => {
        console.log(`[${field}] Sending to ${result.rows.length} webhooks.`);
        let sentCount = 0;
        let deletedCount = 0;
        let promises = result.rows.map(function (row) {
            const hook = new Webhook(row.url);
            hook.setUsername('FFXIV Timers');
            return hook.send(embed).then(() => sentCount++).catch(function(err) {
                if (err.toString().indexOf('10015')) {
                    deletedCount++;
                    client.query(`DELETE FROM ffxivtimers WHERE id = $1`, [row.id], (err) => {
                        if (err) console.error(err);
                    });
                } else {
                    console.error(err);
                }
            });
        });
        Promise.all(promises).then(() => {console.log(`[${field}] ${sentCount} sent, ${deletedCount} deleted.`)});
    });
}
function oceanCron() {
    let nextTime = new Date(crons.ocean.nextDates().format('x')*1).getTime();
    let routeIndex = (Math.floor(nextTime / 7200000) + 16) % 72;
    let route = oceanRoutes[oceanPattern[routeIndex]];
    let nextRouteIndex = (Math.floor(nextTime / 7200000) + 17) % 72;
    let nextRoute = oceanRoutes[oceanPattern[nextRouteIndex]];
    let embed = new MessageBuilder()
        .setTitle('Ocean Fishing in 15 minutes')
        .addField('This route', route)
        .addField('Next route in 2 hours', nextRoute)
        .setThumbnail(iconUrls.ocean)
        .setColor(3857630)
        .setURL('https://ffxivtimers.com/ocean');
    sendWebhooks('ocean', embed);
}

function generateJumboCron(region) {
    return function () {
        let dbcolumn = 'jumbona', fieldName, fieldValue;
        if (region === 'na') {
            dbcolumn = 'jumbona';
            fieldName = 'NA Data Centers';
            fieldValue = 'Aether, Primal, & Crystal';
        } else if (region === 'eu') {
            dbcolumn = 'jumboeu';
            fieldName = 'EU Data Centers';
            fieldValue = 'Chaos & Light';
        } else if (region === 'jp') {
            dbcolumn = 'jumbojp';
            fieldName = 'JP Data Centers';
            fieldValue = 'Elemental, Gaia, & Mana';
        } else return;
        let embed = new MessageBuilder()
            .setTitle('Jumbo Cactpot')
            .addField(fieldName, fieldValue)
            .setThumbnail(iconUrls.jumbo)
            .setColor(15179565)
            .setURL('https://ffxivtimers.com/');
        sendWebhooks(dbcolumn, embed);
    }
}
function weeklyCron() {
    let embed = new MessageBuilder()
        .setTitle('Weekly Reset in 1 hour')
        .setDescription('Raid lockouts\nCurrency limits\nChallenge log\nCustom deliveries\nWondrous Tails\nSquadron priority missions\nMasked Carnivale')
        .setThumbnail(iconUrls.weekly)
        .setColor(8904250)
        .setURL('https://ffxivtimers.com/');
    sendWebhooks('weekly', embed);
}
function dailyCron() {
    let embed = new MessageBuilder()
        .setTitle('Daily Reset in 1 hour')
        .setDescription('Daily roulettes\nDaily quests\nBeast tribes\nFrontlines')
        .setThumbnail(iconUrls.daily)
        .setColor(11156190)
        .setURL('https://ffxivtimers.com/');
    sendWebhooks('daily', embed);
}
function gcCron() {
    let embed = new MessageBuilder()
        .setTitle('Grand Company and Rowena Daily Reset in 30 minutes')
        .setDescription('Squadron training\nGrand Company supply & provisioning turn-ins\nRowena collectable turn-ins')
        .setThumbnail(iconUrls.gc)
        .setColor(14563898)
        .setURL('https://ffxivtimers.com/');
    sendWebhooks('gc', embed);
}
function lalaCron() {
    let embed = new MessageBuilder()
        .setTitle('Lalachievements rankings have updated')
        .setDescription('Achievement, minion, and mount collections\nRaid clear dates')
        .setThumbnail(iconUrls.lala)
        .setColor(26282)
        .setURL('https://www.lalachievements.com/en/ranking/achievement/global/');
    sendWebhooks('lala', embed);
}
function isTriadNow(date) {
    return Math.floor(((date.getTime() - 460800000) / 604800000) % 2) === 0;
}
// todo test tt formula more
function ttCron() {
    //let ttNow = Math.floor(((new Date().getTime() - 460800000) / 604800000) % 2) === 0;
    if (isTriadNow(new Date())) {
        let embed = new MessageBuilder()
            .setTitle('A Triple Triad tournament has begun')
            .setThumbnail(iconUrls.jumbo)
            .setColor(15179565)
            .setURL('https://ffxivtimers.com/');
        sendWebhooks('tt', embed);
    }
}
function ttEndCron() {
    //let ttNow = Math.floor(((new Date().getTime() - 460800000) / 604800000) % 2) === 0;
    if (isTriadNow(new Date())) {
        let embed = new MessageBuilder()
            .setTitle('A Triple Triad tournament is ending in 1 hour')
            .setThumbnail(iconUrls.jumbo)
            .setColor(15179565)
            .setURL('https://ffxivtimers.com/');
        sendWebhooks('tt', embed);
    }
}
// verminion is every 8 days why does se do this
function isVerminionNow(date) {
    return Math.floor(((date.getTime() - 86400000) / 86400000) % 8) === 0;
}
function isVerminionEndNow(date) {
    return Math.floor(((date.getTime() - (86400000*4)) / 86400000) % 8) === 0;
}
function verminionCron() {
    //let tNow = Math.floor(((new Date().getTime() - 86400000) / 86400000) % 8) === 0;
    if (isVerminionNow(new Date())) {
        let embed = new MessageBuilder()
            .setTitle('A Verminion tournament has begun')
            .setThumbnail(iconUrls.jumbo)
            .setColor(15179565)
            .setURL('https://ffxivtimers.com/');
        sendWebhooks('tt', embed);
    }
}
function verminionEndCron() {
    //let tNow = Math.floor(((new Date().getTime() - (86400000*4)) / 86400000) % 8) === 0;
    if (isVerminionEndNow(new Date())) {
        let embed = new MessageBuilder()
            .setTitle('A Verminion tournament is ending in 1 hour')
            .setThumbnail(iconUrls.jumbo)
            .setColor(15179565)
            .setURL('https://ffxivtimers.com/');
        sendWebhooks('tt', embed);
    }
}
function verminionTest() {
    let nexts = crons.verminionEndWarning.nextDates(30);
    nexts.forEach(next => {
        let date = new Date(next.format('x')*1 - (86400000*29)+50);
        let tNow = Math.floor(((date - (86400000*4)) / 86400000) % 8) === 0;
        console.log(date, tNow);
    });
}
function restartNotify() {
    let embed = new MessageBuilder()
        .setTitle('FFXIV Timers restarted')
        .setThumbnail('https://cdn.discordapp.com/attachments/546520452202364949/719623236740055050/momodi.png')
        .setColor(4906554)
        .setURL('https://ffxivtimers.com/');
    sendWebhooks('test', embed);
}


let crons = {
    jumbona: new Cron('0 2 * * 0', generateJumboCron('na'), null, true, 'UTC'),
    jumboeu: new Cron('0 19 * * 6', generateJumboCron('eu'), null, true, 'UTC'),
    jumbojp: new Cron('0 12 * * 6', generateJumboCron('jp'), null, true, 'UTC'),
    weekly: new Cron('0 8 * * 2', function() {}, null, true, 'UTC'),
    weeklyWarning: new Cron('0 7 * * 2', weeklyCron, null, true, 'UTC'),
    daily: new Cron('0 15 * * *', function() {}, null, true, 'UTC'),
    dailyWarning: new Cron('0 14 * * *', dailyCron, null, true, 'UTC'),
    gc: new Cron('0 20 * * *', function() {}, null, true, 'UTC'),
    gcWarning: new Cron('30 19 * * *', gcCron, null, true, 'UTC'),
    ocean: new Cron('0 0,2,4,6,8,10,12,14,16,18,20,22 * * *', function() {}, null, true, 'UTC'),
    oceanWarning: new Cron('45 1,3,5,7,9,11,13,15,17,19,21,23 * * *', oceanCron, null, true, 'UTC'),
    lalachievements: new Cron('10 0 * * *', lalaCron, null, true, 'UTC'),
    tt: new Cron('0 8 * * 2', ttCron, null, true, 'UTC'),
    ttEndWarning: new Cron('0 7 * * 2', ttEndCron, null, true, 'UTC'),
    verminion: new Cron('0 15 * * *', verminionCron, null, true, 'UTC'),
    verminionEndWarning: new Cron('45 13 * * *', verminionEndCron, null, true, 'UTC'),
};

app.listen(port, () => console.log(`i am awive on powt ${port} nya`));

//verminionTest();
//lalaCron();
restartNotify();
